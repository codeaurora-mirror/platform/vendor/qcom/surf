LOCAL_PATH := $(call my-dir)

file := $(TARGET_OUT_KEYLAYOUT)/halibut_keypad.kl
ALL_PREBUILT += $(file)
$(file) : $(LOCAL_PATH)/halibut_keypad.kl | $(ACP)
	$(transform-prebuilt-to-target)

include $(CLEAR_VARS)
LOCAL_SRC_FILES := halibut_keypad.kcm
include $(BUILD_KEY_CHAR_MAP)

# to build the bootloader you need the common boot stuff,
# the architecture specific stuff, and the board specific stuff
include vendor/qcom/surf/boot/Android.mk
# include bootloader/legacy/Android.mk

include vendor/qcom/surf/AndroidBoard.mk

